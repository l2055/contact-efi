package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ContactServiceTest {
    private ContactService contactService = new ContactService();

    @Test
    void shouldFailTooShort() {
        assertThrows(IllegalArgumentException.class, () -> contactService.creerContact("ab"));
    }

    @Test
    void shouldFailTooLong() {
        assertThrows(IllegalArgumentException.class, () -> contactService.creerContact("abcdefghijk"));
    }

    @Test
    void shouldFailNull() {
        assertThrows(IllegalArgumentException.class, () -> contactService.creerContact(null));
    }

    @Test
    void shouldNotFound() {
        assertThrows(ContactNotFoundException.class, () -> contactService.supprimerContact("Julien"));
    }

    @Test
    void shouldPassDelete() {
        contactService.creerContact("Julien");
        contactService.supprimerContact("Julien");
    }
}
