/*
 * package com.example;
 * 
 * import org.junit.jupiter.api.Assertions;
 * import org.junit.jupiter.api.Test;
 * import org.junit.jupiter.api.extension.ExtendWith;
 * import org.mockito.InjectMocks;
 * import org.mockito.Mock;
 * import org.mockito.Mockito;
 * import org.mockito.junit.jupiter.MockitoExtension;
 * 
 * @ExtendWith(MockitoExtension.class)
 * public class ContactServiceMockTest {
 * 
 * @Mock
 * private IContactDao contactDao;
 * 
 * @InjectMocks
 * private ContactService contactService = new ContactService();
 * 
 * @Test
 * void shouldFailDuplicate() {
 * Mockito.when(contactDao.isContactExist("thierry")).thenReturn(true);
 * 
 * Assertions.assertThrows(ContactDuplicateException.class,
 * () -> contactService.creerContact("thierry"));
 * }
 * 
 * @Test
 * void shouldPass() {
 * Mockito.when(contactDao.isContactExist("thierry")).thenReturn(false);
 * contactService.creerContact("thierry");
 * }
 * }
 */