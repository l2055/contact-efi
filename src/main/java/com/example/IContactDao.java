package com.example;

public interface IContactDao {
    boolean isContactExist(String nom);

    void addContact(String nom);

    void delete(String nom);
}
