package com.example;

import java.util.ArrayList;
import java.util.List;

public class ContactService {

    private IContactDao contactDao = new ContactDao();

    List<String> contacts = new ArrayList<>();

    public void creerContact(String nom) {

        if (nom == null || nom.trim().length() < 3 || nom.trim().length() > 10) {
            throw new IllegalArgumentException();
        }

        if (contactDao.isContactExist(nom)) {
            throw new IllegalArgumentException();
        }

        contactDao.addContact(nom);
    }

    public void supprimerContact(String nom) {
        if (!contactDao.isContactExist(nom)) {
            throw new ContactNotFoundException();
        }

        contactDao.delete(nom);
    }
}
