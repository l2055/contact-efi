package com.example;

public class ContactNotFoundException extends RuntimeException {

    public ContactNotFoundException() {
        super("Le contact n'existe pas");
    }

}
